
# 参考资料

## 思维导图

我们简单梳理了一下该项目可能涉及的资料，并制作成了思维导图，欢迎下载查阅。

* [riscv-linux.xmind](https://gitee.com/tinylab/riscv-linux/raw/master/refs/riscv-linux.xmind)

## 更多信息

该目录下的相关资料、链接等收集自网络，仅供学习和研究使用，版权归原作者所有，使用时请务必遵循相关知识产权。

* 邮件列表
    * <https://lore.kernel.org/linux-riscv/>

* RVOS 视频课程
    * Code: <https://gitee.com/tinylab/rvos-lab>
    * Video: <https://www.bilibili.com/video/BV1Q5411w7z5>

* Linux 移植到新架构
    * Porting Linux to a new processor architecture (kernel itself)
        * [part 1: The basics](https://lwn.net/Articles/654783/)，[译文](https://tinylab.org/lwn-654783/) @通天塔
        * [part 2: The early code](https://lwn.net/Articles/656286/), [译文](https://tinylab.org/lwn-656286/) @通天塔
        * [part 3: To the finish line](https://lwn.net/Articles/657939/), [译文](https://tinylab.org/lwn-657939/) @通天塔
    * [Base porting linux kernel riscv archiecture](https://elinux.org/images/c/c7/Base-porting-linux-kernel-riscv-archiecture-ELC-2019.pdf)
    * [Porting Linux to a new architecture (not only kernel)](https://lwn.net/Articles/597351/), [pdf](https://elinux.org/images/5/50/Rybczynska_Porting_Linux_to_a_new_architecture_ELC2014.pdf)

* RISC-V 汇编
    * [在 Linux Lab Disk 下开展 RISC-V 汇编语言实验](https://zhuanlan.zhihu.com/p/479295461)
    * [RISC-V ASSEMBLY LANGUAGE Programmer Manual Part I](https://shakti.org.in/docs/risc-v-asm-manual.pdf)
    * [Introduction to Assembly: RISC-V Instruction Set Architecture](https://inst.eecs.berkeley.edu/~cs61c/sp21/pdfs/docs/lectures/lec06_assembly.key.pdf)
    * [RISC-V Assembly Programmer's Manual](https://github.com/riscv-non-isa/riscv-asm-manual/blob/master/riscv-asm.md)

* RISC-V 标准与规范
    * [RISC-V Platform Specification](https://github.com/riscv/riscv-platform-specs/blob/main/riscv-platform-spec.adoc)
    * [RISC-V ABIs Specification](https://github.com/riscv-non-isa/riscv-elf-psabi-doc/blob/master/riscv-abi.adoc)
        * [The RISC-V psABI](https://courses.cs.washington.edu/courses/cse481a/20sp/notes/psabi.pdf)
    * [RISC-V Supervisor Binary Interface Specification](https://github.com/riscv-non-isa/riscv-sbi-doc/blob/master/riscv-sbi.adoc)
    * [RISC-V UEFI Protocols](https://github.com/riscv-non-isa/riscv-uefi/releases)
    * [RISC-V Instruction Set Manual](https://github.com/riscv/riscv-isa-manual)
        * [Volume I: User-Level ISA](https://github.com/riscv/riscv-isa-manual/releases/download/Ratified-IMAFDQC/riscv-spec-20191213.pdf)
        * [Volume II: Privileged Architecture](https://github.com/riscv/riscv-isa-manual/releases/download/Priv-v1.12/riscv-privileged-20211203.pdf)
    * Extensions
        * [External Debug Support](https://github.com/riscv/riscv-debug-spec/)
        * [Trace Specification](https://github.com/riscv-non-isa/riscv-trace-spec/)
        * [RISC-V Cryptography Extension](https://github.com/riscv/riscv-crypto)
        * [RISC-V Vector Specification](https://github.com/riscv/riscv-v-spec/)

* 硬件资料
    * [平头哥开放的 RISC-V 核](https://github.com/orgs/T-head-Semi/repositories?type=all)
    * [平头哥 D1 资料](https://dl.linux-sunxi.org/D1/)

* 在线书籍
    * [The RISC-V Reader: An Open Architecture Atlas](http://www.riscvbook.com/chinese/RISC-V-Reader-Chinese-v2p1.pdf)
    * [Linux Insides](https://0xax.gitbooks.io/linux-insides/content/index.html)

// 4 位的 UInt 寄存器
val reg1 = Reg(UInt(4 bits))

// 有效触发边沿到来时自动 +1 更新的寄存器
val reg2 = RegNext(reg1 + 1)

// 设定好复位值（和初始值）的寄存器
val reg3 = RegInit(U"0000")
reg3 := reg2
when(reg2 === 5) {
  reg3 := 0xF
}

// 条件为真时，采样 reg3 赋予值给 reg4
val reg4 = RegNextWhen(reg3, cond)

// Copyright (C) 2022 Wu Zhangjin <falcon@ruma.tech>, All Rights Reserved.
//
// Gcc Inline Assembly: https://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
//                      https://www.cristal.univ-lille.fr/~marquet/ens/ctx/doc/l-ia.html
//                      https://wiki.osdev.org/Inline_assembly

// X86_64 ISA:          https://www.aldeid.com/wiki/X86-assembly/Instructions

#include "microbench.h"
#include "common.cc"
